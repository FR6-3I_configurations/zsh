<div align='center'>

# Zsh

</div>

## 📦 Dependencies (Arch)

### ❗ Required

Official :

```shell
paru -S \
  starship \
  zoxide \
  zsh \
  zsh-autosuggestions \
  zsh-syntax-highlighting
```

AUR :

```shell
paru -S macchina-bin
```

### ➕ Optional

Official :

```shell
paru -S \
  bandwhich \
  bat \
  bat-extras \
  bottom \
  cargo-audit \
  cargo-outdated \
  cargo-watch \
  choose \
  dog \
  dust \
  dysk \
  eva \
  eza \
  fd \
  git \
  gping \
  helix \
  hyperfine \
  man-pages-fr \
  mandoc \
  neovim \
  onefetch \
  perl-image-exiftool \
  podman \
  pipewire-pulse \
  pulsemixer \
  procs \
  ripgrep \
  rustup \
  sd \
  skim \
  tokei \
  wmctrl \
  yazi \
  zsh-completions
```

AUR :

```shell
paru -S \
  fnm-bin \
  joshuto-bin \
  mullvad-vpn-bin \
  trashy
```

## ⚙️ Configuration

Add Pnpm dependency (with NodeJS) :

```shell
corepack enable
coprepack prepare pnpm@latest --activate
```

Symbolic link to inform Zsh about the configuration :

```shell
ln -s ~/.config/zsh/.zshrc ~/.zshrc
```

Make Zsh the shell by default :

```shell
chsh -s /usr/bin/zsh
```

Start Zsh :

```shell
zsh
```
