# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Prompt ZSH
autoload -Uz promptinit
promptinit

# Hooks ZSH
autoload -Uz add-zsh-hook

# Environment variables
export EDITOR="helix"
export GPG_TTY="$(tty)"
export PATH="$PATH:$HOME/.cargo/bin/"
export NEXT_TELEMETRY_DISABLED=1

# Add some scripts
source "/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
source "/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
source "$HOME/.config/zsh/completion-for-pnpm.zsh"

# Initialise some tools
eval "$(starship init zsh)"
eval "$(zoxide init zsh)"
eval "$(fnm env --use-on-cd)"

# Run OneFetch for every entry in a Git repository
last_repository=
check_directory_for_new_repository() {
	current_repository=$(git rev-parse --show-toplevel 2> /dev/null)

	if [ "$current_repository" ] && \
	   [ "$current_repository" != "$last_repository" ]; then
		onefetch
	fi
	last_repository=$current_repository
}

show_code_stats() {
	check_directory_for_new_repository
}

add-zsh-hook chpwd show_code_stats

# Launches these commands automatically when the terminal is opened
macchina
check_directory_for_new_repository

# In color
alias ip="ip -c"

# Alternatives
alias bc="eva"
alias cat="bat"
alias cd="z"
alias cloc="tokei"
alias cut="choose"
alias df="dysk"
alias dig="dog"
alias drill="dog"
alias du="dust"
alias find="fd"
alias fzf="sk"
alias grep="rg"
alias htop="btm"
alias ls="eza -a --icons"
alias man="batman"
alias ping="gping"
alias ps="procs"
alias sed="sd"
alias top="btm"
alias time="hyperfine"

# Shortcuts
alias bw="sudo bandwhich"
alias cs="choose"
alias dogl="dog A AAAA NS MX TXT"
alias dust="dust -r"
alias et="exiftool"
alias hf="hyperfine"
alias hx="helix"
alias jt="joshuto"
alias mc="macchina"
alias nv="nvim"
alias of="onefetch"
alias pmx="pulsemixer"
alias rgb="batgrep"
alias tk="tokei"
alias x="eza -a --icons"
alias xl="eza -a --icons -l -h --git"
alias xlt="eza -a --icons -l -h --git -T -I .git --git-ignore"
alias xt="eza -a --icons -T -I .git --git-ignore"
alias yz="yazi"

# Cargo shortcuts
alias cg="cargo"
alias cga="cargo audit"
alias cgc="cargo clippy"
alias cgcf="cargo clippy --fix --allow-dirty"
alias cgca="cargo clippy -- --force-warn clippy::cargo -F clippy::complexity -F clippy::correctness --force-warn clippy::nursery --force-warn clippy::pedantic -F clippy::perf -F clippy::style -F clippy::suspicious"
alias cgcaf="cargo clippy --fix --allow-dirty -- --force-warn clippy::cargo -F clippy::complexity -F clippy::correctness --force-warn clippy::nursery --force-warn clippy::pedantic -F clippy::perf -F clippy::style -F clippy::suspicious"
alias cgd="cargo doc"
alias cgi="cargo install"
alias cgo="cargo outdated"
alias cgr="cargo run"
alias cgrr="cargo run --release"
alias cgt="cargo test"
alias cgw="cargo watch"

# Git shortcuts
alias gta="git add"
alias gtb="git branch"
alias gtbc="git branch -v | rg '\[disparue\]' | choose 0 | xargs git branch -D"
alias gtc="git commit -m"
alias gtca="git commit --amend --no-edit"
alias gtcn="git clone"
alias gtco="git checkout"
alias gtd="git diff"
alias gti="git init"
alias gtpl="git pull"
alias gtps="git push"
alias gtpso="git push -u origin"
alias gtr="git restore --staged"
alias gtra="git remote add"
alias gts="git status"

# Podman shortcuts
alias pm="podman"
alias pmb="podman build"
alias pmi="podman image"
alias pmr="podman run"

# Mullvad shortcuts
alias mul="mullvad"
alias mulc="mullvad connect -w"
alias muld="mullvad disconnect -w"
alias mulr="mullvad reconnect -w"
alias muls="mullvad status"

# Trashy shortcuts
alias te="trash empty"
alias tea="trash empty --all"
alias tl="trash list"
alias tp="trash put"
alias tr="trash restore"
alias tra="trash restore --all"
